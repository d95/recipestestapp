package com.esfer0.omletapp.main.fragments.recipe.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.esfer0.omletapp.main.datas.RecipeData;
import com.esfer0.omletapp.main.fragments.recipe.views.RecipeViewHolder;
import com.testapp.esfer0.omletapp.R;

import java.util.ArrayList;
import java.util.List;


public class RecipeAdapter extends BaseAdapter {
    private List<RecipeData> recipeList = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public RecipeAdapter(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    @Override
    public int getCount() {
        return recipeList.size();
    }

    @Override
    public RecipeData getItem(int i) {
        return recipeList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View resultView = view;

        final RecipeData recipeData = getItem(i);

        if (view == null) {
            resultView = inflateView();

            RecipeViewHolder viewQuestion = new RecipeViewHolder(resultView);
            viewQuestion.fillView(recipeData, layoutInflater.getContext());
            resultView.setTag(viewQuestion);
        } else {
            RecipeViewHolder viewQuestion = (RecipeViewHolder) resultView.getTag();
            viewQuestion.fillView(recipeData, layoutInflater.getContext());
        }

        return resultView;
    }

    private View inflateView() {
        return layoutInflater.inflate(R.layout.view_recipe_list, null);
    }

    public void setRecipeList(List<RecipeData> recipeList) {
        this.recipeList.clear();
        this.recipeList.addAll(recipeList);
        notifyDataSetChanged();
    }
}
