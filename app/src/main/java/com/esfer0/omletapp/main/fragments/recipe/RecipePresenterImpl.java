package com.esfer0.omletapp.main.fragments.recipe;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.esfer0.omletapp.base.app.RecipeApplication;
import com.esfer0.omletapp.main.datas.RecipeData;
import com.testapp.esfer0.omletapp.R;

import java.util.List;

public class RecipePresenterImpl implements RecipePresenter {
    private static final String DEFOULT_NAME = "omelet";

    private RecipeView view;
    private RecipeModel model;

    public RecipePresenterImpl(RecipeView view) {
        this.view = view;

        model = new RecipeModelImpl();
    }

    @Override
    public void loadData() {
        if (isOnline()) {
            view.showRefresh();

            model.loadData();
        } else {
            view.showMsg(R.string.network_state_error);
            view.updateRecipeList(model.getDataDb(DEFOULT_NAME));
            view.hideRefresh();
        }
    }

    @Override
    public void searchData(String name) {
        if (isOnline()) {
            view.showRefresh();

            model.searchData(name);
        } else {
//            view.showMsg(R.string.network_state_error);
            view.updateRecipeList(model.getDataDb(name));
            view.hideRefresh();
        }
    }

    @Override
    public void showData() {
        List<RecipeData> list = model.getDataDb(DEFOULT_NAME);

        if (list.size() > 0) {
            view.updateRecipeList(list);
            view.hideRefresh();
        } else {
            loadData();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) RecipeApplication.get().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void onDestroy() {
        model.onDestroy();
    }
}
