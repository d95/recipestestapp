package com.esfer0.omletapp.main.utils.retrofit;

import com.esfer0.omletapp.Const;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RecipeApi {

    @GET(Const.URL.DEFAULT)
    Call<Result> getAllRecipe();

    @GET(Const.URL.SEARCH)
    Call<Result> searchRecipe(@Query("q") String name);
}
