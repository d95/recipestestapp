package com.esfer0.omletapp.main.fragments.recipe;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.esfer0.omletapp.Const;
import com.esfer0.omletapp.base.BaseFragment;
import com.esfer0.omletapp.main.datas.RecipeData;
import com.esfer0.omletapp.main.fragments.recipe.adapter.RecipeAdapter;
import com.esfer0.omletapp.main.utils.eventbase.EventBaseWrapper;
import com.esfer0.omletapp.main.utils.eventbase.subscription.NetErrorResultSubscription;
import com.esfer0.omletapp.main.utils.eventbase.subscription.RecipeResultSubscription;
import com.testapp.esfer0.omletapp.R;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;

public class RecipeFragment extends BaseFragment implements RecipeView, SwipeRefreshLayout.OnRefreshListener,
        RecipeResultSubscription, NetErrorResultSubscription{

    @BindView(R.id.searchEditText)
    EditText searchEditText;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recipeListView)
    ListView recipeListView;

    private RecipePresenter presenter;
    private RecipeAdapter adapter;

    private Toast toast;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new RecipePresenterImpl(this);
        adapter = new RecipeAdapter(getActivityLayoutInflater());

        getBaseActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void initViews() {
        recipeListView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(this);

        presenter.loadData();

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().trim().equals("")) {
                    presenter.showData();

                    return;
                }

                presenter.searchData(editable.toString().trim());
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recipes;
    }

    @Override
    public void onRefresh() {
        searchEditText.setText("");
        presenter.loadData();
    }

    @Override
    public void onStart() {
        super.onStart();

        EventBaseWrapper.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        EventBaseWrapper.getInstance().unregister(this);
    }

    @Override
    @Subscribe()
    public void onEvent(@NonNull List<RecipeData> list) {
        updateRecipeList(list);

        hideRefresh();
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(@NonNull Throwable t) {

        showMsg(R.string.net_error);
        hideRefresh();
    }

    @Override
    public void showRefresh() {
        if(swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void hideRefresh() {
        if(swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void updateRecipeList(List<RecipeData> list) {
        if (list == null) return;

        adapter.setRecipeList(list);
    }

    @Override
    public void showMsg(int msg) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(getApplicationContext(), getText(msg), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();

        super.onDestroy();
    }

    private void logi(String text) {
        Log.i(Const.APP.TAG, this.getClass().getSimpleName() + " " + text);
    }
}
