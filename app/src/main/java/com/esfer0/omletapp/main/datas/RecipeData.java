package com.esfer0.omletapp.main.datas;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class RecipeData {

    private int id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("href")
    @Expose
    private String href;

    @SerializedName("ingredients")
    @Expose
    private String ingredients;

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
        public String toString() {
            return new ToStringBuilder(this).append("title", title).append("href", href).append("ingredients", ingredients).append("thumbnail", thumbnail).toString();
        }
}
