package com.esfer0.omletapp.main.fragments.recipe;

import com.esfer0.omletapp.main.datas.RecipeData;

import java.util.List;

public interface RecipeView {
    void showRefresh();
    void hideRefresh();
    void updateRecipeList(List<RecipeData> data);

    void showMsg(int msg);
}
