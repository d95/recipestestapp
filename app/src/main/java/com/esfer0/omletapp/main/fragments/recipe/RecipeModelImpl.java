package com.esfer0.omletapp.main.fragments.recipe;

import android.util.Log;

import com.esfer0.omletapp.Const;
import com.esfer0.omletapp.main.datas.RecipeData;
import com.esfer0.omletapp.main.datas.RecipeDataManager;
import com.esfer0.omletapp.main.utils.eventbase.EventBaseWrapper;
import com.esfer0.omletapp.main.utils.retrofit.NetworkClient;
import com.esfer0.omletapp.main.utils.retrofit.RecipeApi;
import com.esfer0.omletapp.main.utils.retrofit.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeModelImpl implements RecipeModel {
    private NetworkClient networkClient;
    private RecipeApi api;
    private RecipeDataManager dataManager;

    public RecipeModelImpl() {
        networkClient = new NetworkClient();
        dataManager = new RecipeDataManager();
    }

    @Override
    public void loadData() {
        api = networkClient.getRetrofit().create(RecipeApi.class);

        Call<Result> call = api.getAllRecipe();

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                logi("onResponse " + call.isExecuted());

                if (call.isExecuted()) {
                    EventBaseWrapper.getInstance().post(response.body().getRecipeData());

                    dataManager.insertData(response.body().getRecipeData());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                logi("onFailure " + t.getMessage());

                EventBaseWrapper.getInstance().post(t);
            }
        });
    }

    @Override
    public void searchData(String name) {
        api = networkClient.getRetrofit().create(RecipeApi.class);

        Call<Result> call = api.searchRecipe(name);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                logi("onResponse " + call.isExecuted());

                if (call.isExecuted()) {
                    EventBaseWrapper.getInstance().post(response.body().getRecipeData());

                    dataManager.insertData(response.body().getRecipeData());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                logi("onFailure " + t.getMessage());

                EventBaseWrapper.getInstance().post(t);
            }
        });
    }

    @Override
    public List<RecipeData> getDataDb(String name) {
        return dataManager.getAllByName(name);
    }

    @Override
    public void onDestroy() {

    }

    private void logi(String text) {
        Log.i(Const.APP.TAG, this.getClass().getSimpleName() + " " + text);
    }
}
