package com.esfer0.omletapp.main.utils.eventbase.subscription;

import android.support.annotation.NonNull;

import com.esfer0.omletapp.main.datas.RecipeData;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public interface NetErrorResultSubscription {
    @Subscribe()
    void onEvent(@NonNull Throwable t);
}
