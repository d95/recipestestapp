package com.esfer0.omletapp.main.utils.retrofit;

import com.esfer0.omletapp.main.datas.RecipeData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("results")
    @Expose
    private List<RecipeData> recipeData = null;

    public List<RecipeData> getRecipeData() {
        return recipeData;
    }

    public void setRecipeData(List<RecipeData> recipeData) {
        this.recipeData = recipeData;
    }

}
