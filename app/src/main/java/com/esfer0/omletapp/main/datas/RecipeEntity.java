package com.esfer0.omletapp.main.datas;

public class RecipeEntity {
    public static final String TABLE_NAME = "recipe";

    public static final String COLUMN_ID = "_id_recipe";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_HREF = "href";
    public static final String COLUMN_INGREDIENTS = "ingredients";
    public static final String COLUMN_THUMBNAIL= "thumbnail";

    public static final String[] ALL_COLUMNS = {
            COLUMN_ID,
            COLUMN_TITLE,
            COLUMN_HREF,
            COLUMN_INGREDIENTS,
            COLUMN_THUMBNAIL
    };
}
