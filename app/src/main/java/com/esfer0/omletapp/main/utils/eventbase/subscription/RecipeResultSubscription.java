package com.esfer0.omletapp.main.utils.eventbase.subscription;

import android.support.annotation.NonNull;

import com.esfer0.omletapp.main.datas.RecipeData;
import com.esfer0.omletapp.main.datas.RecipeDataConverter;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public interface RecipeResultSubscription {
    @Subscribe()
    void onEvent(@NonNull List<RecipeData> list);
}
