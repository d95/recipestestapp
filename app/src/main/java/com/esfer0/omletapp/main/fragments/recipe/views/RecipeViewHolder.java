package com.esfer0.omletapp.main.fragments.recipe.views;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.esfer0.omletapp.Const;
import com.esfer0.omletapp.base.app.RecipeApplication;
import com.esfer0.omletapp.main.datas.RecipeData;
import com.esfer0.omletapp.main.utils.NetworkUtils;
import com.testapp.esfer0.omletapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecipeViewHolder {
    @BindView(R.id.conteinerView)
    View conteinerView;

    @BindView(R.id.titleRecipeTextView)
    TextView titleTextView;

    @BindView(R.id.imageView)
    CircleImageView imageView;

    @BindView(R.id.contentRecipeTextView)
    TextView contentRecipeTextView;


    public RecipeViewHolder(View view) {
        ButterKnife.bind(this, view);
    }

    public void fillView(final RecipeData recipeData, final Context context) {
        titleTextView.setText(recipeData.getTitle().trim());
        contentRecipeTextView.setText(recipeData.getIngredients().trim());

        conteinerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NetworkUtils.openUrl(recipeData.getHref(), context);
            }
        });

        Glide.clear(imageView);

        Glide.with(RecipeApplication.get())
                .load(String.valueOf(recipeData.getThumbnail()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.recipe_img)
                .into(imageView);
    }
}
