package com.esfer0.omletapp.main.datas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class RecipeDataManager {
    private SQLiteDatabase mSqLiteDatabase;
    private DbHelper dbHelper;

    public RecipeDataManager() {
        this.dbHelper = new DbHelper();
    }

    public void insertData(List<RecipeData> models) {
        for (RecipeData data: models) {
            mSqLiteDatabase = dbHelper.getWritableDatabase();
            ContentValues values = RecipeDataConverter.toContentValues(data);
            mSqLiteDatabase.insert(RecipeEntity.TABLE_NAME, null, values);
        }

        dbHelper.close();
    }

    public void insertData(RecipeData model) {
        mSqLiteDatabase = dbHelper.getWritableDatabase();
        ContentValues values = RecipeDataConverter.toContentValues(model);
        mSqLiteDatabase.insert(RecipeEntity.TABLE_NAME, null, values);
        dbHelper.close();
    }

    public void deleteAll() {
        mSqLiteDatabase = dbHelper.getWritableDatabase();
        mSqLiteDatabase.delete(RecipeEntity.TABLE_NAME,
                null,
                null);
        dbHelper.close();
    }

    public List<RecipeData> getAllByName(String name) {
        mSqLiteDatabase = dbHelper.getWritableDatabase();
        List<RecipeData> list = new ArrayList<>();

        Cursor cursor = mSqLiteDatabase.query(RecipeEntity.TABLE_NAME,
                RecipeEntity.ALL_COLUMNS,
                RecipeEntity.COLUMN_TITLE + " LIKE ?", new String[] {"%" + name + "%"},
                null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            RecipeData model = RecipeDataConverter.toModel(cursor);
            list.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        dbHelper.close();
        return list;
    }

    public List<RecipeData> getAll() {
        mSqLiteDatabase = dbHelper.getWritableDatabase();
        List<RecipeData> list = new ArrayList<>();

        Cursor cursor = mSqLiteDatabase.query(RecipeEntity.TABLE_NAME,
                RecipeEntity.ALL_COLUMNS,
                null,
                null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            RecipeData model = RecipeDataConverter.toModel(cursor);
            list.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        dbHelper.close();
        return list;
    }
}
