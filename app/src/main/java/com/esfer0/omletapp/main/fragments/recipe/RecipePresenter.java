package com.esfer0.omletapp.main.fragments.recipe;

import com.esfer0.omletapp.main.datas.RecipeData;

import java.util.List;

public interface RecipePresenter {
    void loadData();
    void searchData(String name);
    void showData();

    void onDestroy();
}
