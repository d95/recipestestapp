package com.esfer0.omletapp.main.datas;

import android.content.ContentValues;
import android.database.Cursor;

public class RecipeDataConverter {
    public static ContentValues toContentValues(RecipeData model) {
        ContentValues values = new ContentValues();
        values.put(RecipeEntity.COLUMN_TITLE, model.getTitle());
        values.put(RecipeEntity.COLUMN_HREF, model.getHref());
        values.put(RecipeEntity.COLUMN_INGREDIENTS, model.getIngredients());
        values.put(RecipeEntity.COLUMN_THUMBNAIL, model.getThumbnail());

        return values;
    }

    public static RecipeData toModel(Cursor cursor) {
        RecipeData model = new RecipeData();
        model.setId(cursor.getInt(0));
        model.setTitle(cursor.getString(1));
        model.setHref(cursor.getString(2));
        model.setIngredients(cursor.getString(3));
        model.setThumbnail(cursor.getString(4));

        return model;
    }
}
