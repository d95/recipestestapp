package com.esfer0.omletapp.main.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.esfer0.omletapp.base.app.RecipeApplication;

public class NetworkUtils {
    public static void openUrl(String url, Context context) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }
}
