package com.esfer0.omletapp.main;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.esfer0.omletapp.base.BaseActivity;
import com.esfer0.omletapp.main.fragments.recipe.RecipeFragment;
import com.testapp.esfer0.omletapp.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initActionBar();

        replaceFragment(RecipeFragment.class, null);
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.view_action_bar);
    }
}
