package com.esfer0.omletapp.main.fragments.recipe;

import com.esfer0.omletapp.main.datas.RecipeData;

import java.util.List;

public interface RecipeModel {
    void loadData();

    void searchData(String name);

    List<RecipeData> getDataDb(String name);

    void onDestroy();
}
