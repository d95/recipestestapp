package com.esfer0.omletapp.main.datas;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.esfer0.omletapp.base.app.RecipeApplication;

public class DbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "recipe.db";
    private static final int DATABASE_VERSION = 2;

    public DbHelper() {
        super(RecipeApplication.get(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_RECIPE_TABLE = "CREATE TABLE " + RecipeEntity.TABLE_NAME + "(" +
                RecipeEntity.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                RecipeEntity.COLUMN_TITLE + " TEXT, " +
                RecipeEntity.COLUMN_HREF + " TEXT, " +
                RecipeEntity.COLUMN_INGREDIENTS + " TEXT, " +
                RecipeEntity.COLUMN_THUMBNAIL + " TEXT, " +
                "UNIQUE (\"" + RecipeEntity.COLUMN_TITLE + "\") ON CONFLICT IGNORE);";

        sqLiteDatabase.execSQL(SQL_CREATE_RECIPE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RecipeEntity.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
