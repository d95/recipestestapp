package com.esfer0.omletapp.main.utils.retrofit;

import com.esfer0.omletapp.Const;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {

    public static Retrofit retrofit;

    public Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Const.URL.BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

       return retrofit;
    }

}
