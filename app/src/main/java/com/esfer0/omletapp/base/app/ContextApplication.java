package com.esfer0.omletapp.base.app;

import android.app.Application;
import android.support.annotation.NonNull;

public abstract class ContextApplication extends Application {
    private static final class Holder {
        private static ContextApplication APPLICATION;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Holder.APPLICATION = this;
    }

    @NonNull
    public static ContextApplication get() {
        if (Holder.APPLICATION == null) {
            throw new NullPointerException();
        }

        return Holder.APPLICATION;
    }
}
