package com.esfer0.omletapp.base;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.WindowManager;

import com.testapp.esfer0.omletapp.R;


public class BaseActivity extends ViewActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void replaceFragment(@NonNull Class<? extends Fragment> fragmentClass, Bundle arguments) {
        try {
            Fragment fragment = fragmentClass.newInstance();

            fragment.setArguments(arguments);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    protected BaseActivity getBaseActivity() {
        return  this;
    }
}
