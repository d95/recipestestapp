package com.esfer0.omletapp.base;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ObservedFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        return rootView;
    }

    protected LayoutInflater getActivityLayoutInflater() {
        return getActivity().getLayoutInflater();
    }
    protected int getColorFromRes(int resId) {
        return getBaseActivity().getColorFromRes(resId);
    }

    protected Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }

    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    protected Resources.Theme getTheme() {
        return getActivity().getTheme();
    }
}
