package com.esfer0.omletapp.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class ViewFragment extends ObservedFragment {
    protected boolean isViewExists = false;
    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflateRootView(inflater, container);
        unbinder = ButterKnife.bind(this, rootView);

        initViews();

        isViewExists = true;

        return rootView;
    }

    private View inflateRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    protected abstract int getLayoutId();

    protected void initViews() {
        // to override
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isViewExists = false;

        if (unbinder != null) unbinder.unbind();
    }
}
