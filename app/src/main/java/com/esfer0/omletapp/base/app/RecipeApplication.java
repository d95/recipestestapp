package com.esfer0.omletapp.base.app;


import android.content.Context;
import android.util.Log;
import android.view.View;

public class RecipeApplication extends ContextApplication {
    public static final int UI_VISABILITY_FLAG = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
