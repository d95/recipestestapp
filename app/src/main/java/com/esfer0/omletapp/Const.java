package com.esfer0.omletapp;

public interface Const {
    interface APP {
        String TAG= "Esfer0";
    }

    interface URL {
        String BASE = "http://www.recipepuppy.com";
        String DEFAULT = "/api/?i=onions,garlic&q=omelet&p=3";
        String SEARCH = "/api/";
    }
}
